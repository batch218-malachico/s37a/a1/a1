
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoute = require("./routes/userRoute.js");
const courseRoutes = require("./routes/productRoutes.js");


const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoute);
app.use("/products", courseRoutes);


mongoose.connect("mongodb+srv://admin:admin@b218-coursebooking.xtvlb8f.mongodb.net/?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to Malachico-Mongo DB Atlas.'));

app.listen(process.env.PORT || 4000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

