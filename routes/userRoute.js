
const express = require("express");

const router = express.Router();

const auth = require("../auth.js");

const User = require("../models/user.js");

const userController = require("../controllers/userController.js");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController))
});

router.post("/details", (req, res) => {
	userController.userDetails(req.body).then(
		resultFromController => res.send(resultFromController))
});

router.post("/checkout", auth.verify, (req, res) => {
    let data = {
        userId: auth.decode(req.headers.authorization).id,
        productId: req.body.productId
    }

    userController.checkout(data).then(result => res.send(result));
});



module.exports = router;